
import React, { Fragment, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom'

const Subscripcion = () => {

    const { id } = useParams();

    const [ subscripcion, setSubscripcion ] = useState([]);

    useEffect(() => {
        getPaquetes();
    }, [])

    const getPaquetes = async () => {
        const data =  await fetch('./../assets/data.json');
        const subscripcionRes = (await data.json()).objModel;
        let items = [];
        for (let index = 0; index < 8; index++) {
            const item = subscripcionRes[Math.floor(Math.random()*subscripcionRes.length)];
            items.push(item);
        }
        console.log(items);
        setSubscripcion([...items]);
    }

    return (
        <Fragment>
            <div className="list-group my-4">
                {subscripcion.map( subs => (
                    <div className="list-group-item list-group-item-action" key={Math.random()}>
                        <div className="d-flex w-100 justify-content-between">
                            <h5 className="mb-1">
                                {subs.namePackage}
                            </h5>
                            <small className="text-muted">
                                ${subs.quote}
                            </small>
                        </div>
                        <p className="mb-1">
                            {subs.quoteDescription}
                        </p>
                        <small className="text-muted">
                            {new Date(subs.nextExpiration).toLocaleDateString()}
                        </small>
                    </div>
                ) )}
            </div>
        </Fragment>
    );

}

export default Subscripcion;
