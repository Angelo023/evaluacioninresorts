
import React, { Fragment, useEffect, useState } from 'react';

import { Link } from "react-router-dom";

const Paquete = () => {

    const [ paquetes, setPaquetes ] = useState([]);

    useEffect(() => {
        getPaquetes();
    }, [])

    const getPaquetes = async () => {

        const data =  await fetch('assets/suscription.json');
        const paquetesRes = await data.json();
        setPaquetes([...paquetesRes.objModel]);
    }

    return (
        <Fragment>
            <div className="row">
                { paquetes.map( paquete => (
                    <div className="col-sm-12 col-md-4 my-2" key={Math.random()}>
                        <div className="card">
                            <div className="card-body">
                                <Link to={`/detalles/${paquete.id}`} className="card-title h5">
                                    {paquete.package.name}
                                </Link>
                                <p className="card-text">
                                    {paquete.package.description}
                                </p>
                            </div>
                            <div className="card-footer">
                                <p>
                                    {new Date(paquete.creationDate).toLocaleDateString()}
                                </p>
                                <footer className="blockquote-footer">{paquete.package.duration}</footer>
                            </div>
                        </div>
                    </div>
                ) ) }
            </div>
        </Fragment>
    );

}

export default Paquete;
